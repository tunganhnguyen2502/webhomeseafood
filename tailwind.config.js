module.exports = {
  // purge: [],
  purge: ['./pages/**/*.{js,ts,jsx,tsx}', './src/components/**/*.{js,ts,jsx,tsx}'],
  darkMode: 'class', // or 'media' or 'class' or false
  theme: {
    // extend: {},ß
    fontFamily: {
      SFProDisplay: ['SFProDisplay'],
      SFProDisplayBold: ['SFProDisplayBold'],
      SFProDisplayRegular: ['SFProDisplayRegular'],
      UTM_Penumbra: ['UTM_Penumbra'],
      UTM_PenumbraBold: ['UTM_PenumbraBold'],
    },
    screens: {
      sm: '600px',
      sm1: '768px',
      md: '960px',
      lg: '1280px',
      xl: '1920px',
    },
    fontSize: {
      xs: '.75rem', // 12px
      sm: '.875rem', // 14px
      base: '1rem', // 16px
      lg: '1.125rem', // 18px
      xl: '1.25rem', // 20px
      xl1: '1.375rem', // 22px
      xl2: '1.5rem', // 24px
      xl3: '1.625rem', // 26px
      xl4: '1.75rem', // 28px
      xl5: '1.875rem', // 30px
      xl6: '2.25rem', // 36px
      xl7: '3rem',
      xl8: '4rem',
      xl9: '5rem',
    },
    colors: {
      primaryColor: '#FF6020',
      primaryHover: '#F04E0D',
      primaryStroke: '#F05213',
      primaryBg: '#ff6020',
      lightOrange: '#FFAF8D',
      black20: '#B0B0B0',
      black50: '#858585',
      black70: '#535353',
      black75: '#3D3D3D',
      black80: '#292929',
      black90: '#1E1E1E',
      black95: '#151515',
      black100: '#000000',
      white0: '#FFFFFF',
      white: '#FFF9F9',
      white90: '#ECEAE7',
      white95: '#F1F1F1',
      red: '#DD2929',
      transparent: 'transparent',
    },
    minWidth: {
      5: '1.25rem',
      6: '1.5rem',
      12: '3rem',
      14: '3.5rem',
      36: '9rem',
      '4/5': '80%',
    },
    minHeight: {
      5: '1.25rem',
      6: '1.5rem',
      12: '3rem',
      14: '3.5rem',
      36: '9rem',
      '4/5': '80%',
    },
    maxHeight: {
      '4/5': '80%',
      full: '100%',
    },
    lineHeight: {
      3.125: '3.125rem',
    },
  },
  variants: {
    extend: {
      borderWidth: ['hover', 'focus'],
      borderColor: ['disabled', 'hover', 'focus', 'active'],
      backgroundColor: ['disabled', 'hover', 'focus', 'active'],
      backgroundOpacity: ['disabled', 'hover', 'focus', 'active'],
      textOpacity: ['disabled', 'hover', 'focus', 'active'],
      opacity: ['disabled', 'hover', 'focus', 'active'],
      pointerEvents: ['disabled', 'hover', 'focus', 'active'],
      fontWeight: ['hover', 'focus'],
    },
  },
  // plugins: [],
};
