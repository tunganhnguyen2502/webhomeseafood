import Footer from '@/components/Common/Footer';
import Header from '@/components/Common/Header';
import ProductTemplate from '@/components/Layout/ProductTemplate';
import UnsplashBackground from '@/components/molecules/UnsplashBackground/UnsplashBackground';
import HomeBanner from '@/components/organisms/HomeBanner';
import HomeCommentSlide from '@/components/organisms/HomeCommentSlide';
import type { NextPage } from 'next';
import Head from 'next/head';
import React from 'react';

const Main: NextPage = () => {
  return (
    <div className="mt-40 mb-10">
      <Head>
        <title>HomeSeaFood - Trang chủ</title>
      </Head>
      <Header />
      <HomeBanner />
      <ProductTemplate />
      <HomeCommentSlide />
      <UnsplashBackground />
      <Footer />
    </div>
  );
};

export default Main;
