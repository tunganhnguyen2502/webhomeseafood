import { LANGUAGE_LOCAL } from '@/constants/data';
import { CssBaseline } from '@material-ui/core';
import type { AppProps } from 'next/app';
import React, { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { Provider } from 'react-redux';
import { store } from 'src/apps/store';
import 'tailwindcss/tailwind.css';
import '../src/assets/styles/globals.css';
import '../src/assets/styles/index.css';
import '../src/components/languageI18Next';

function MyApp({ Component, pageProps }: AppProps) {
  const { i18n } = useTranslation('common');
  const _translation = (key: string) => i18n.changeLanguage(key);

  useEffect(() => {
    const langLocal = localStorage.getItem(LANGUAGE_LOCAL);
    if (!langLocal) {
      localStorage.setItem(LANGUAGE_LOCAL, 'vi');
      _translation('vi');
    } else _translation(langLocal);

    // On page load or when changing themes, best to add inline in `head` to avoid FOUC
    if (
      localStorage.theme === 'dark' ||
      (!('theme' in localStorage) && window.matchMedia('(prefers-color-scheme: dark)').matches)
    ) {
      document.documentElement.classList.add('dark');
    } else {
      document.documentElement.classList.remove('dark');
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <React.Fragment>
      <Provider store={store}>
        <div className="dark:bg-black90 bg-white95">
          <Component {...pageProps}>
            <CssBaseline />
          </Component>
        </div>
      </Provider>
    </React.Fragment>
  );
}

export default MyApp;
