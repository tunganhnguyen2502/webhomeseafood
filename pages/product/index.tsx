import Footer from '@/components/Common/Footer';
import Header from '@/components/Common/Header';
import ProductTemplate from '@/components/Layout/ProductTemplate';
import UnsplashBackground from '@/components/molecules/UnsplashBackground';
import Head from 'next/head';
import React from 'react';

const ProductPage = () => {
  return (
    <div className="mt-40 mb-10">
      <Head>
        <title>HomeSeaFood - Sản phẩm</title>
      </Head>
      <Header />
      <ProductTemplate />
      <UnsplashBackground />
      <Footer />
    </div>
  );
};

export default ProductPage;
