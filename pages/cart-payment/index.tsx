import Footer from '@/components/Common/Footer';
import Header from '@/components/Common/Header';
import CartPaymentTemplate from '@/components/Layout/CartPaymentTemplate';
import Head from 'next/head';
import React from 'react';

const CartPaymentPage = () => {
  return (
    <div className="mt-40 pb-40">
      <Head>
        <title>HomeSeaFood - Giỏ hàng</title>
      </Head>
      <Header />
      <CartPaymentTemplate />
      <Footer />
    </div>
  );
};

export default CartPaymentPage;
