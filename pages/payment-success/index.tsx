import Footer from '@/components/Common/Footer';
import Header from '@/components/Common/Header';
import PaymentSuccessTemplate from '@/components/Layout/PaymentSuccessTemplate';
import Head from 'next/head';
import React from 'react';

const PaymentSuccessPage = () => {
  return (
    <div className="mt-40 pb-40" style={{ height: 'calc(100vh - 10rem)' }}>
      <Head>
        <title>Thanh toán thành công</title>
      </Head>
      <Header />
      <PaymentSuccessTemplate />
      <Footer />
    </div>
  );
};

export default PaymentSuccessPage;
