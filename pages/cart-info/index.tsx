import Footer from '@/components/Common/Footer';
import Header from '@/components/Common/Header';
import CartInfoTemplate from '@/components/Layout/CartInfoTemplate';
import Head from 'next/head';
import React from 'react';

const CartInfoPage = () => {
  return (
    <div className="mt-40 pb-40">
      <Head>
        <title>HomeSeaFood - Giỏ hàng</title>
      </Head>
      <Header />
      <CartInfoTemplate />
      <Footer />
    </div>
  );
};

export default CartInfoPage;
