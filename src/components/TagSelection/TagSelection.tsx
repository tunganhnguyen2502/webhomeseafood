import { APP_LAYOUT_THEME, COLORS } from '@/constants/themes';
import React from 'react';
import styled, { css } from 'styled-components';

interface TagSelectionProps {
  icon: React.ImgHTMLAttributes<ImageData>;
  iconActive: React.ImgHTMLAttributes<ImageData>;
  title?: string;
  active?: boolean;
  onClick?: () => void;
  appLayoutTheme: string;
}

const TagSelection: React.FC<TagSelectionProps> = ({
  icon,
  iconActive,
  title = 'title',
  active = false,
  onClick,
  appLayoutTheme,
}) => {
  return (
    <Wrapper
      icon={icon}
      iconActive={iconActive}
      active={active}
      onClick={onClick}
      appLayoutTheme={appLayoutTheme}
    >
      <div className="icon" />
      <p className="title ml-3 text-lg font-bold">{title}</p>
    </Wrapper>
  );
};

const Wrapper = styled.div<TagSelectionProps>`
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
  padding: 0.55rem 0;
  border-radius: 5px;
  width: 14.5rem;
  ${(props) =>
    props.appLayoutTheme === APP_LAYOUT_THEME.dark && props.active
      ? css`
          background-color: ${COLORS.black95};
        `
      : props.appLayoutTheme === APP_LAYOUT_THEME.dark && !props.active
      ? css`
          background-color: ${COLORS.black80};
        `
      : props.appLayoutTheme === APP_LAYOUT_THEME.light && props.active
      ? css`
          background: linear-gradient(180deg, #ff7945 0%, #f04e0d 85.94%);
        `
      : props.appLayoutTheme === APP_LAYOUT_THEME.light &&
        !props.active &&
        css`
          background-color: ${COLORS.white0};
        `}

  .title {
    width: 6rem;
    color: ${(props) =>
      props.appLayoutTheme === APP_LAYOUT_THEME.dark && props.active
        ? COLORS.primaryColor
        : props.appLayoutTheme === APP_LAYOUT_THEME.dark && !props.active
        ? COLORS.white
        : props.appLayoutTheme === APP_LAYOUT_THEME.light && props.active
        ? COLORS.white
        : COLORS.black50};
  }

  .icon {
    height: 3.5rem;
    width: 3.5rem;

    ${(props) =>
      props.active
        ? css`
            background-image: url(${props.iconActive.src});
          `
        : css`
            background-image: url(${props.icon.src});
          `}
    background-repeat: no-repeat;
    background-size: cover;
  }

  &:hover {
    /* background-color: ${COLORS.black95}; */

    ${(props) =>
      props.appLayoutTheme === APP_LAYOUT_THEME.dark
        ? css`
            background-color: ${COLORS.black95};
          `
        : props.appLayoutTheme === APP_LAYOUT_THEME.light &&
          css`
            background: linear-gradient(180deg, #ff7945 0%, #f04e0d 85.94%);
          `}
    transition: 0.15s all ease-in-out;
    .title {
      color: ${COLORS.primaryColor};
      transition: 0.15s all ease-in-out;

      color: ${(props) =>
        props.appLayoutTheme === APP_LAYOUT_THEME.dark
          ? COLORS.primaryColor
          : props.appLayoutTheme === APP_LAYOUT_THEME.light && COLORS.white};
    }
    .icon {
      background-image: url(${(props) => props.iconActive.src});
      transition: 0.15s all ease-in-out;
    }
  }
`;

export default TagSelection;
