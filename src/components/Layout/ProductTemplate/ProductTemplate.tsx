import ProductCard from '@/components/molecules/ProductCard';
import TabCategorySelection from '@/components/molecules/TabCategorySelection';
import { images } from '@/constants/images';
import { Container } from '@material-ui/core';
import Image from 'next/image';
import React from 'react';
import { Wrapper } from './styles';

const ProductTemplate = ({}) => {
  return (
    <Wrapper>
      <Container maxWidth="xl">
        <section>
          <div className="w-full flex justify-center items-center">
            <figure className="flex items-center">
              <Image className="img__food" src={images.decor_line_left} alt="food1" />
            </figure>
            <p className="uppercase text-xl6 font-UTM_PenumbraBold mx-4 dark:text-white text-primaryColor transition">
              Deal hot mỗi ngày
            </p>
            <figure className="flex items-center">
              <Image className="img__food" src={images.decor_line_right} alt="food1" />
            </figure>
          </div>
          <div className="mt-12 w-full flex justify-center items-center">
            <div className="mx-2">
              <ProductCard imgProduct={images.gettyimages1.src} />
            </div>
            <div className="mx-2">
              <ProductCard imgProduct={images.gettyimages2.src} disabled />
            </div>
            <div className="mx-2">
              <ProductCard imgProduct={images.gettyimages3.src} />
            </div>
            <div className="mx-2">
              <ProductCard imgProduct={images.gettyimages4.src} />
            </div>
          </div>
          <div className="flex justify-center items-center mt-10">
            <div className="flex justify-center items-center cursor-pointer">
              <p className="mr-2 mt-1 text-black50">Xem Thêm</p>
              <Image src={images.chevron_down} alt="chevron_down" />
            </div>
          </div>
        </section>
        <section className="mt-32">
          <div className="w-full flex justify-center items-center">
            <figure className="flex items-center">
              <Image className="img__food" src={images.decor_line_left} alt="food1" />
            </figure>
            <p className="dark:text-white text-primaryColor transition uppercase text-xl6 font-UTM_PenumbraBold mx-4">
              Sản Phẩm
            </p>
            <figure className="flex items-center">
              <Image className="img__food" src={images.decor_line_right} alt="food1" />
            </figure>
          </div>

          <div className="w-full flex justify-center items-center mt-10">
            <TabCategorySelection />
          </div>

          <div className="mt-12 w-full flex justify-center items-center">
            <div className="mx-2">
              <ProductCard imgProduct={images.gettyimages1.src} />
            </div>
            <div className="mx-2">
              <ProductCard imgProduct={images.gettyimages2.src} disabled />
            </div>
            <div className="mx-2">
              <ProductCard imgProduct={images.gettyimages3.src} />
            </div>
            <div className="mx-2">
              <ProductCard imgProduct={images.gettyimages4.src} />
            </div>
          </div>
          <div className="mt-12 w-full flex justify-center items-center">
            <div className="mx-2">
              <ProductCard imgProduct={images.gettyimages1.src} />
            </div>
            <div className="mx-2">
              <ProductCard imgProduct={images.gettyimages2.src} />
            </div>
            <div className="mx-2">
              <ProductCard imgProduct={images.gettyimages3.src} disabled />
            </div>
            <div className="mx-2">
              <ProductCard imgProduct={images.gettyimages4.src} />
            </div>
          </div>
          <div className="mt-12 w-full flex justify-center items-center">
            <div className="mx-2">
              <ProductCard imgProduct={images.gettyimages1.src} />
            </div>
            <div className="mx-2">
              <ProductCard imgProduct={images.gettyimages2.src} />
            </div>
            <div className="mx-2">
              <ProductCard imgProduct={images.gettyimages3.src} />
            </div>
            <div className="mx-2">
              <ProductCard imgProduct={images.gettyimages4.src} />
            </div>
          </div>
          <div className="mt-12 w-full flex justify-center items-center">
            <div className="mx-2">
              <ProductCard imgProduct={images.gettyimages1.src} />
            </div>
            <div className="mx-2">
              <ProductCard imgProduct={images.gettyimages2.src} />
            </div>
            <div className="mx-2">
              <ProductCard imgProduct={images.gettyimages3.src} />
            </div>
            <div className="mx-2">
              <ProductCard imgProduct={images.gettyimages4.src} />
            </div>
          </div>
        </section>
      </Container>
    </Wrapper>
  );
};

export default ProductTemplate;
