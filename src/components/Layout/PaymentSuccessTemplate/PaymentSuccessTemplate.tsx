import Button from '@/components/Button';
import { images } from '@/constants/images';
import Image from 'next/image';
import { useRouter } from 'next/router';
import React from 'react';

const PaymentSuccessTemplate = () => {
  const router = useRouter();

  return (
    <section className="flex justify-center items-center w-full h-full">
      <div className="mt-5 flex flex-col justify-center items-center">
        <figure>
          <Image src={images.gif_success} width="150" height="150" alt="gif_success" />
        </figure>
        <p className="dark:text-white text-black90 transition uppercase font-UTM_PenumbraBold text-xl3 mt-12">
          cám ơn bạn đã mua sắm tại home seafood
        </p>
        <p className="dark:text-white text-black70 transition text-center w-5/12 mt-12">
          Mã số đơn hàng của bạn là <span className="text-primaryColor">#12345678</span>. Đơn hàng
          sẽ được xử lý và giao đến bạn trong thời gian sớm nhất. Vui lòng liên hệ Hotline: 0123 456
          789 để được giải quyết mọi thắc mắc
        </p>
        <div className="mt-10">
          <Button
            height="4rem"
            width="14rem"
            fontSize="1.2rem"
            fontWeight="bold"
            text="Tiếp tục mua hàng"
            handleClick={() => router.push('/')}
          />
        </div>
      </div>
    </section>
  );
};

export default PaymentSuccessTemplate;
