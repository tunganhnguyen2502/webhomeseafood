import CartAmount from '@/components/organisms/Cart/CartAmount';
import FormOrderInfo from '@/components/organisms/Cart/FormOrderInfo';
import SideDish from '@/components/organisms/Cart/SideDish';
import { Container, Grid } from '@material-ui/core';
import React from 'react';

const CartInfoTemplate = ({}) => {
  return (
    <Container maxWidth="xl" className="py-5">
      <CartUIDesktop />
    </Container>
  );
};

const CartUIDesktop = () => {
  return (
    <Grid container>
      <Grid item xs={12} lg={7}>
        <div className="pr-4">
          <SideDish />
          <FormOrderInfo />
        </div>
      </Grid>
      <Grid item xs={12} lg={5}>
        <CartAmount />
      </Grid>
    </Grid>
  );
};

export default CartInfoTemplate;
