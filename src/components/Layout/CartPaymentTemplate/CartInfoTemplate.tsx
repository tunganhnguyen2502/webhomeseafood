import CartAmount from '@/components/organisms/Cart/CartAmount';
import FormPayment from '@/components/organisms/Cart/FormPayment';
import { Container, Grid } from '@material-ui/core';
import React from 'react';

const CartPaymentTemplate = ({}) => {
  return (
    <Container maxWidth="xl" className="py-5">
      <CartUIDesktop />
    </Container>
  );
};

const CartUIDesktop = () => {
  return (
    <Grid container>
      <Grid item xs={12} lg={7}>
        <div className="pr-4">
          <FormPayment />
        </div>
      </Grid>
      <Grid item xs={12} lg={5}>
        <CartAmount />
      </Grid>
    </Grid>
  );
};

export default CartPaymentTemplate;
