import Link from 'next/link';
import React from 'react';
import styled from 'styled-components';

interface NavLinkProps {
  href: string;
  name: string;
}

const WrapperLink = styled.a`
  /* color: red; */
`;

const NavLink = ({ href, name }: NavLinkProps) => {
  // Must add passHref to Link
  return (
    <Link href={href} passHref>
      <WrapperLink>{name}</WrapperLink>
    </Link>
  );
};

export default NavLink;
