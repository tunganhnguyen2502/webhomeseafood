import { images } from '@/constants/images';
import useOnClickOutside from '@/hooks/useClickOutside';
import React, { useRef, useState } from 'react';
import styled, { css } from 'styled-components';

interface ItemListData {
  name: string;
}
interface DropdownProps {
  listData: Array<ItemListData>;
  placeholder?: string;
}

const Dropdown = ({ listData = [], placeholder = '---' }: DropdownProps) => {
  const dropdownRef = useRef(null);
  const [showDropdown, setShowDropdown] = useState(false);
  const [selectedItem, setSelectedItem] = useState<ItemListData>();

  // handle show dropdown
  const handleClickOutside = () => setShowDropdown(false);
  const handleClickInside = () => setShowDropdown(!showDropdown);
  useOnClickOutside(dropdownRef, handleClickOutside);

  return (
    <Wrapper
      className="dark:bg-black80 bg-white0 transition flex items-center relative"
      onClick={handleClickInside}
      ref={dropdownRef}
      showDropdown={showDropdown}
    >
      <div className="py-2 px-4 flex justify-between items-center w-full cursor-pointer">
        <p className="title text__item text-black50">
          {selectedItem?.name ? selectedItem.name : placeholder}
        </p>
        <div className="icon_down" />
      </div>
      {showDropdown && listData.length > 0 && (
        <div className="dropdown__list p-3 dark:bg-black80 bg-white0 rounded-b-md absolute">
          {listData.map((x, i) => {
            return (
              <p
                key={i}
                className="text__item text-black20 my-1 hover:text-primaryColor transition"
                onClick={() => setSelectedItem(x)}
              >
                {x.name}
              </p>
            );
          })}
        </div>
      )}
    </Wrapper>
  );
};

interface WrapperProps {
  showDropdown?: boolean;
}

const Wrapper = styled.div<WrapperProps>`
  width: 100%;
  min-width: 11.5rem;
  min-height: 3rem;
  border-radius: 5px;
  .icon_down {
    background-image: url(${images.chevron_down.src});
    width: 1.2rem;
    height: 1.2rem;
    background-size: cover;
    transition: 0.2s all ease-in-out;
    ${(props) =>
      props.showDropdown &&
      css`
        transform: rotate(180deg);
      `};
  }

  .dropdown__list {
    top: 120%;
    width: 100%;
    z-index: 1;
  }

  .title {
    max-width: fit-content;
  }

  .text__item {
    cursor: pointer;
    width: 100%;
    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-line-clamp: 1;
    -webkit-box-orient: vertical;
  }
`;

export default Dropdown;
