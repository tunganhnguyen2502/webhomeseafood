import { COLORS } from '@/constants/themes';
import { useField } from 'formik';
import React from 'react';
import styled from 'styled-components';

interface InputFieldProps {
  name: string;
  label?: React.ReactElement;
  placeholder: string;
  type: string;
  disabled?: boolean;
  maxLength?: number;
  color?: string;
  placeholderColor?: string;
}

const InputField: React.FC<InputFieldProps> = (props) => {
  const { disabled = false, color = COLORS.white, placeholderColor = COLORS.black50 } = props;
  const [field, { error }] = useField(props);

  return (
    <Wrapper color={color} placeholderColor={placeholderColor}>
      {props.label}
      <input
        className="input__field dark:bg-black80 bg-white0 transition"
        {...field}
        {...props}
        disabled={disabled}
        autoComplete="off"
        maxLength={props.maxLength ? props.maxLength : 200}
        inputMode={props.type === 'phone' || props.type === 'number' ? 'numeric' : 'text'}
      />
      {error && <p>{error}</p>}
    </Wrapper>
  );
};

interface WrapperProps {
  color: string;
  placeholderColor: string;
}

export const Wrapper = styled.div<WrapperProps>`
  width: 100%;
  .input__field {
    width: 100%;
    max-height: 3rem;
    height: 3rem;
    border-radius: 5px;
    padding: 0 0.75rem;
    outline: none;
    color: ${(props) => props.color};
    &::placeholder {
      color: ${(props) => props.placeholderColor};
    }
  }
`;

export default InputField;
