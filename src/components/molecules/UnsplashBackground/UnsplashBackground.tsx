import Button from '@/components/Button';
import { images } from '@/constants/images';
import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.div`
  margin-top: 7rem;
  background-image: url(${images.unsplash_bg.src});
  width: 100%;
  height: 26rem;
  background-size: cover;
  .bg__overlay {
    width: 100%;
    height: 100%;
    background-color: rgba(0, 0, 0, 0.5);

    .title {
      width: 26rem;
      max-width: 26rem;
    }
  }
`;

const UnsplashBackground = () => {
  return (
    <Wrapper>
      <div className="bg__overlay flex justify-center items-center flex-col">
        <p className="title font-SFProDisplayBold uppercase text-xl4 text-center">
          ĐĂNG KÝ NGAY ĐỂ ĐƯỢC HƯỞNG NHIỀU ĐẶC QUYỀN ƯU ĐÃI CHO THÀNH VIÊN
        </p>
        <div className="mt-20 flex justify-center items-center">
          <Button
            text="ĐĂNG KÝ"
            width="14rem"
            height="4rem"
            fontSize="1.5rem"
            upperCase
            fontFamily="SFProDisplayBold"
            fontWeight="800"
            handleClick={() => alert(1231313)}
          />
        </div>
      </div>
    </Wrapper>
  );
};

export default UnsplashBackground;
