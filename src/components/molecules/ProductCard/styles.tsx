import { images } from '@/constants/images';
import { APP_LAYOUT_THEME, COLORS } from '@/constants/themes';
import styled, { css } from 'styled-components';

interface Props {
  appLayoutTheme: string;
}

export const Wrapper = styled.div<Props>`
  width: 20rem;
  height: 24.5rem;
  box-sizing: border-box;
  border-radius: 1.1875rem;

  .wrap__img {
    background-image: ${(props) =>
      props.appLayoutTheme === APP_LAYOUT_THEME.dark
        ? css`url(${images.bg_product_darkMode.src});`
        : css`url(${images.bg_product_lightMode.src});`};
    height: 15rem;
    background-repeat: no-repeat;
    background-size: cover;
    border-radius: 1.1875rem 1.1875rem 0 0;

    .product__discount {
      top: 0.75rem;
      right: 1rem;
      background-color: ${COLORS.primaryBg};
      border-radius: 5px;
      padding: 0.75px 7px;
    }
  }

  .product__name {
    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    -webkit-box-orient: vertical;
  }

  .price__base {
    position: relative;
    width: fit-content;
    &::before {
      position: absolute;
      display: block;
      content: '';
      height: 1px;
      width: 100%;
      background-color: ${COLORS.black50};
      top: 50%;
      transform: translateY(-50%);
    }
  }
`;
