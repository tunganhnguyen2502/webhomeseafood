import { useAppSelector } from '@/apps/hook';
import Button from '@/components/Button';
import QuantitySelect from '@/components/QuantitySelect';
import { COLORS } from '@/constants/themes';
import { selectAppLayoutTheme } from '@/features/appLayout/appLayoutSlice';
import { convertNumToMoney } from '@/utils/convertNumberToMoney';
import Image from 'next/image';
import React from 'react';
import { Wrapper } from './styles';

interface Props {
  imgProduct: string;
  basePrice?: number;
  salePrice?: number;
  disabled?: boolean;
}

const ProductCard: React.FC<Props> = ({
  imgProduct,
  basePrice = 150000,
  salePrice = 125000,
  disabled = false,
}) => {
  const appLayoutTheme = useAppSelector(selectAppLayoutTheme);

  const _handleGetValue = (value: number) => {
    console.log(value);
  };

  return (
    <Wrapper
      className="bg-white dark:bg-black95 dark:border dark:border-black-95 dark:border-solid"
      appLayoutTheme={appLayoutTheme}
    >
      <figure
        className={`wrap__img relative flex justify-center items-center ${
          disabled && 'opacity-40'
        }`}
      >
        <Image
          className="img__product"
          src={imgProduct}
          alt="img__product"
          width="300"
          height="250"
          objectFit="contain"
        />
        <div className="product__discount absolute">
          <p className="text-xs font-SFProDisplayBold">-15%</p>
        </div>
      </figure>
      <div className="mt-2 mb-3 mx-3">
        <p className="product__name text-xl dark:text-white text-black70 transition">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit ut
        </p>
        <div className="mt-4 w-full flex justify-between items-center">
          <div>
            <p className="text-xl text-primaryColor">{convertNumToMoney({ number: salePrice })}</p>
            <p className="price__base text-sm text-black50">
              {convertNumToMoney({ number: basePrice })}
            </p>
          </div>
          {disabled ? (
            <div>
              <Button
                text="Tạm hết hàng"
                width="10rem"
                height="3rem"
                borderRadius="2.5rem"
                bgColor={COLORS.black70}
                bgHoverColor={COLORS.black70}
                border="none"
                fontSize="1.25rem"
                disabled
              />
            </div>
          ) : (
            <QuantitySelect defaultQty={0} getValue={_handleGetValue} />
          )}
        </div>
      </div>
    </Wrapper>
  );
};

export default ProductCard;
