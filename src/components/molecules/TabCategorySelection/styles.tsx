import { APP_LAYOUT_THEME, COLORS } from '@/constants/themes';
import styled, { css } from 'styled-components';

export interface StyleWrapper {
  activeColor?: string;
  inActiveColor?: string;
  styleTitle?: {
    colorActive: string;
    colorInActive: string;
  };
  appLayoutTheme?: string;
}

export const Wrapper = styled.div<StyleWrapper>`
  display: flex;
  justify-content: center;
  align-items: center;

  .tag__container {
    cursor: pointer;
    padding: 0.8rem 1.2rem;
    border-radius: 2.5rem;
    margin: 0 0.5rem;

    ${(props) =>
      props.appLayoutTheme === APP_LAYOUT_THEME.dark
        ? css`
            &.active {
              background-color: ${props.activeColor || COLORS.white};
              .title {
                color: ${props.styleTitle?.colorActive || COLORS.black80};
              }
            }
            &.inactive {
              background-color: ${props.inActiveColor || COLORS.black80};
              .title {
                color: ${props.styleTitle?.colorInActive || COLORS.black50};
              }
            }
            &:hover {
              transition: 0.15s all ease-in-out;
              background-color: ${props.activeColor || COLORS.white};
              .title {
                color: ${props.styleTitle?.colorActive || COLORS.black80};
              }
            }
          `
        : css`
            &.active {
              background: ${props.activeColor ||
              'linear-gradient(180deg, #FF7945 0%, #F04E0D 85.94%);'};
              .title {
                color: ${props.styleTitle?.colorActive || COLORS.white90};
              }
            }
            &.inactive {
              background-color: ${props.inActiveColor || COLORS.white90};
              .title {
                color: ${props.styleTitle?.colorInActive || COLORS.black50};
              }
            }
            &:hover {
              transition: 0.15s all ease-in-out;
              background: ${props.activeColor ||
              'linear-gradient(180deg, #FF7945 0%, #F04E0D 85.94%);'};
              .title {
                color: ${props.styleTitle?.colorActive || COLORS.white90};
              }
            }
          `}
  }
`;
