import React, { useState } from 'react';

import { StyleWrapper, Wrapper } from './styles';
import { arrTitleTabCategorySelection, IArrTabCategory } from './data';
import { useAppSelector } from '@/apps/hook';
import { selectAppLayoutTheme } from '@/features/appLayout/appLayoutSlice';

interface Props extends StyleWrapper {
  getIdActive?: (id: number) => number;
}

const TabCategorySelection: React.FC<Props> = ({}) => {
  const appLayoutTheme = useAppSelector(selectAppLayoutTheme);
  const [active, setActive] = useState<IArrTabCategory>(arrTitleTabCategorySelection[0]);

  return (
    <Wrapper appLayoutTheme={appLayoutTheme}>
      {arrTitleTabCategorySelection.map((x) => {
        return (
          <div
            className={`tag__container ${x.id === active.id ? 'active' : 'inactive'}`}
            key={x.id}
            onClick={() => setActive(x)}
          >
            <p className="title">{x.title}</p>
          </div>
        );
      })}
    </Wrapper>
  );
};

export default TabCategorySelection;
