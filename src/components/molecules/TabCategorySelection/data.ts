export interface IArrTabCategory {
  id: number;
  title: string;
}

export const arrTitleTabCategorySelection: IArrTabCategory[] = [
  {
    id: 0,
    title: 'Tất cả',
  },
  {
    id: 1,
    title: 'Ốc, sò',
  },
  {
    id: 2,
    title: 'Tôm cua cá ghẹ',
  },
  {
    id: 3,
    title: ' Bạch tuộc, mực ống',
  },
];
