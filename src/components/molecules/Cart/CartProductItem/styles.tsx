import { images } from '@/constants/images';
import { APP_LAYOUT_THEME, COLORS } from '@/constants/themes';
import styled from 'styled-components';

interface Props {
  appLayoutTheme: string;
}

export const Wrapper = styled.div<Props>`
  width: 36rem;
  min-width: 36rem;
  height: 8.5rem;
  max-height: 8.5rem;
  border-radius: 0.5rem;
  display: flex;
  transition: 0.15s;

  .wrap__img {
    width: 13rem;
    min-width: 8rem;
    height: 8.5rem;

    background-image: ${(props) =>
      props.appLayoutTheme === APP_LAYOUT_THEME.dark
        ? `url(${images.bg_product_darkMode.src})`
        : `url(${images.bg_product_lightMode.src})`};

    background-repeat: no-repeat;
    background-size: cover;
    border-radius: 0.5rem 0 0 0.5rem;
    .product__discount {
      top: 0;
      right: 0;
      background-color: ${COLORS.primaryBg};
      border-radius: 5px 0 0 5px;
      padding: 0.75px 7px;
    }
  }

  .block__info {
    .desc {
      overflow: hidden;
      text-overflow: ellipsis;
      display: -webkit-box;
      -webkit-line-clamp: 2;
      -webkit-box-orient: vertical;
    }

    .price__base {
      position: relative;
      width: fit-content;
      &::before {
        position: absolute;
        display: block;
        content: '';
        height: 1px;
        width: 100%;
        background-color: ${COLORS.black50};
        top: 50%;
        transform: translateY(-50%);
      }
    }
  }
`;
