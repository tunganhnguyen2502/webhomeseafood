import { useAppSelector } from '@/apps/hook';
import Button from '@/components/Button';
import QuantitySelect from '@/components/QuantitySelect';
import { images } from '@/constants/images';
import { COLORS } from '@/constants/themes';
import { selectAppLayoutTheme } from '@/features/appLayout/appLayoutSlice';
import { convertNumToMoney } from '@/utils/convertNumberToMoney';
import Image from 'next/image';
import * as React from 'react';
import { Wrapper } from './styles';

export interface ICartProductItemProps {
  imgProduct: string;
  defaultQty?: number;
  className?: string;
  basePrice?: number;
  salePrice?: number;
  disabled?: boolean;
  isDarkStyle?: boolean;
}

const CartProductItem: React.FC<ICartProductItemProps> = ({
  className,
  imgProduct = images.gettyimages3.src,
  basePrice = 150000,
  salePrice = 125000,
  disabled = false,
  isDarkStyle = true,
  defaultQty = 0,
}) => {
  const appLayoutTheme = useAppSelector(selectAppLayoutTheme);

  /**
   * Function
   */
  const _handleGetValue = (value: number) => {
    console.log('value: ', value);
  };

  return (
    <Wrapper className={'dark:bg-black95 bg-white0 ' + className} appLayoutTheme={appLayoutTheme}>
      <figure
        className={`wrap__img flex justify-center items-center relative ${
          disabled ? 'opacity-40' : ''
        }`}
      >
        <Image
          className="img__product"
          src={imgProduct}
          alt="img__product"
          width="185"
          height="125"
          objectFit="contain"
        />
        <div className="product__discount absolute">
          <p className="text-md font-bold font-SFProDisplayBold">-15%</p>
        </div>
      </figure>

      <div className="block__info py-2 px-3 flex flex-col justify-between">
        <p className="desc text-black50 text-xl">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit ut
        </p>
        <div className="flex justify-between items-center mt-2 mr-3">
          <div>
            <p className="text-xl dark:text-white text-black70 transition font-SFProDisplayBold">
              {convertNumToMoney({ number: salePrice })}
            </p>
            <p className="price__base text-black70 text-lg font-SFProDisplayRegular">
              {convertNumToMoney({ number: basePrice })}
            </p>
          </div>
          {disabled ? (
            <div>
              <Button
                text="Tạm hết hàng"
                width="10.5rem"
                height="3rem"
                borderRadius="2.5rem"
                bgColor={COLORS.black70}
                bgHoverColor={COLORS.black70}
                border="none"
                fontSize="1.25rem"
                disabled
              />
            </div>
          ) : (
            <QuantitySelect
              defaultQty={defaultQty}
              getValue={_handleGetValue}
              isDarkStyle={isDarkStyle}
            />
          )}
        </div>
      </div>
    </Wrapper>
  );
};

export default CartProductItem;
