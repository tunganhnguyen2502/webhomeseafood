import React from 'react';
import { Wrapper } from './styles';

interface PanelHeaderExtendProps {
  test?: boolean;
}

const PanelHeaderExtend: React.FC<PanelHeaderExtendProps> = ({ test = false }) => {
  console.log(test);

  return (
    <Wrapper>
      <div className="extend__header flex flex-col md:flex-row justify-center items-center py-2">
        <div className=" wrap flex justify-between items-center w-full">
          <div>apb</div>
          <div>
            <p>Đăng nhập/Đăng ký</p>
          </div>
        </div>
      </div>
    </Wrapper>
  );
};

export default PanelHeaderExtend;
