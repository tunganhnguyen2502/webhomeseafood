import React from 'react';
import styled from 'styled-components';
import { Container } from '@material-ui/core';
import Image from 'next/image';

import { COLORS } from '@/constants/themes';
import { images } from '@/constants/images';

export const Wrapper = styled.footer`
  width: 100%;
  min-height: 2.5rem;
  position: fixed;
  bottom: 0;
  z-index: 11;
  border-top: 1px solid ${COLORS.black100};
`;
const Footer = ({}) => {
  return (
    <Wrapper className="flex items-center dark:bg-black90 bg-white95 py-3 sm:py-1">
      <Container maxWidth="xl">
        <section className="w-full h-full flex flex-col sm:flex-row justify-between items-center">
          <div className="flex justify-center sm:justify-start items-center">
            <p className="cursor-pointer text-black70 text-xs">Chính sách mua hàng</p>
            <p className="cursor-pointer text-black70 text-xs ml-3">Bảo mật thông tin</p>
          </div>
          <div className="flex justify-center items-center">
            <p className="cursor-pointer text-black70 text-xs">Facebook</p>
            <p className="cursor-pointer text-black70 text-xs ml-3">Instagram</p>
          </div>

          <div className="flex justify-center sm:justify-end items-center">
            <figure className="w-6 flex items-center">
              <Image src={images.ic_clock_orange} alt="ic_clock" />
            </figure>
            <p className="text-primaryColor text-xs">Giờ hoạt động 8:00 - 21:00</p>
          </div>
        </section>
      </Container>
    </Wrapper>
  );
};

export default Footer;
