import { useAppSelector } from '@/apps/hook';
import { images } from '@/constants/images';
import { APP_LAYOUT_THEME, COLORS } from '@/constants/themes';
import { selectAppLayoutTheme } from '@/features/appLayout/appLayoutSlice';
import useDarkMode from '@/hooks/useDarkMode';
import Image from 'next/image';
import NextLink from 'next/link';
import React, { useState } from 'react';
import { IoIosSearch } from 'react-icons/io';
import useOnClickOutside from 'src/hooks/useClickOutside';
import { menuNav } from './data';
import { Wrapper } from './styles';

const Header = ({}) => {
  const appLayoutTheme = useAppSelector(selectAppLayoutTheme);

  const [showDropdown, setShowDropdown] = useState(false);
  const [collapse, setCollpase] = useState<boolean>(false);
  const dropdownRef = React.useRef(null);
  const { toggleDarkMode } = useDarkMode();

  const handleClickOutside = () => setShowDropdown(false);
  const handleClickInside = () => setShowDropdown(!showDropdown);
  useOnClickOutside(dropdownRef, handleClickOutside);

  return (
    <Wrapper collapse={collapse} showDropdown={showDropdown}>
      <header>
        <div className="header flex justify-between items-center px-12 py-2 dark:bg-black95 bg-white0 transition">
          <NextLink href="/">
            <figure className="wrap__logo flex items-center cursor-pointer">
              <Image
                src={
                  appLayoutTheme === APP_LAYOUT_THEME.dark
                    ? images.logo_darkMode
                    : images.logo_lightMode
                }
                alt="logo_darkMode"
              />
            </figure>
          </NextLink>
          <ul className="menu hidden md:flex justify-center items-center px-4 py-2 mr-10 relative dark:bg-black80 bg-white90 transition">
            <li
              className="nav__li px-3 font-SFProDisplayRegular cursor-pointer relative"
              onClick={toggleDarkMode}
            >
              <p className={`text-black20 hover:text-primaryStroke transition-colors`}>Switch UI</p>
            </li>
            {menuNav.map((x, index) => {
              return (
                <li
                  className="nav__li px-3 font-SFProDisplayRegular cursor-pointer relative"
                  key={index}
                >
                  <NextLink href={x.href}>
                    <p
                      className={`dark:text-black20 text-black90 hover:text-primaryStroke transition ${
                        index === menuNav.length - 1 ? 'pr-5' : ''
                      }`}
                    >
                      {x.label}
                    </p>
                  </NextLink>
                  {!!x.child && (
                    <div className="dropdown absolute dark:bg-black100 bg-white0">
                      {x.child.map((k, j) => {
                        return (
                          <p
                            key={j}
                            className="text-right dark:text-black50 dark:hover:text-white text-black50 hover:text-black90 transition font-SFProDisplayRegular py-1"
                          >
                            {k.label}
                          </p>
                        );
                      })}
                    </div>
                  )}
                </li>
              );
            })}

            <NextLink href="/cart-info">
              <div className="wrap__cart absolute rounded-full cursor-pointer flex justify-center items-center">
                <div className="icon relative">
                  <span className="span absolute rounded-full bg-white flex justify-center items-center text-sm">
                    1
                  </span>
                </div>
              </div>
            </NextLink>
          </ul>
          <input className="menu-btn" type="checkbox" id="menu-btn" />
          <label className="menu-icon" htmlFor="menu-btn" onClick={() => setCollpase(!collapse)}>
            <span className="navicon" />
          </label>
        </div>

        <ul className="collapse flex md:hidden flex-col justify-center items-start px-16">
          {menuNav.map((x, index) => {
            return (
              <li className="py-2 cursor-pointer" key={index}>
                <NextLink href={x.href}>
                  <p>{x.label}</p>
                </NextLink>
              </li>
            );
          })}
        </ul>
      </header>
      <div className="extend__header dark:bg-black90 bg-white90 transition flex flex-col md:flex-row justify-center items-center py-4">
        <section className="wrap flex justify-center items-center w-full">
          <div className="block__input flex items-center dark:bg-black75 bg-white0 transition rounded-sm w-full p-3">
            <div
              className="container__dropdown cursor-pointer flex justify-between items-center select-none relative"
              onClick={handleClickInside}
              ref={dropdownRef}
            >
              <p className="font-bold text-black20">Tất cả</p>
              <div className="icon_down" />
              {showDropdown && (
                <div className="dropdown__list p-3 dark:bg-black75 bg-white0 transition rounded-b-md absolute">
                  <p className=" dark:text-black20 text-black70 my-1 hover:text-primaryColor hover:font-bold transition">
                    Ốc, sò
                  </p>
                  <p className=" dark:text-black20 text-black70 my-1 hover:text-primaryColor hover:font-bold transition">
                    Tôm, cua, ghẹ, cá
                  </p>
                  <p className=" dark:text-black20 text-black70 my-1 hover:text-primaryColor hover:font-bold transition">
                    Bạch tuộc, mực ống
                  </p>
                </div>
              )}
            </div>

            <div className="w-px h-6 bg-black20 mx-3" />
            <div className="w-full relative">
              <input
                className="w-full dark:bg-black75 bg-white0 dark:text-white0 text-black70 transition"
                placeholder="Tìm theo tên sản phẩm"
              />
              <IoIosSearch
                color={COLORS.white}
                className="icon_search absolute cursor-pointer"
                size="1.35rem"
              />
            </div>
          </div>
          <div className="ml-5">
            <p className="cursor-pointer w-max dark:text-white text-black90 transition">
              Đăng nhập/ <span>Đăng ký</span>
            </p>
          </div>
        </section>
      </div>
    </Wrapper>
  );
};

export default Header;
