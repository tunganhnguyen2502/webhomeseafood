import { images } from '@/constants/images';
import { COLORS, SIZES } from '@/constants/themes';
import styled, { css } from 'styled-components';

interface Props {
  collapse: boolean;
  showDropdown: boolean;
}

export const Wrapper = styled.nav<Props>`
  position: fixed;
  width: 100%;
  z-index: 11;
  top: 0;
  transition: 0.3s all ease-in-out;

  .header {
    box-shadow: 1px 1px 4px 0 rgba(0, 0, 0, 0.1);

    .wrap__logo {
      width: 8.5rem;
    }

    .menu {
      box-sizing: border-box;
      border-radius: 50px;

      .nav__li {
        .dropdown {
          transition: 0.15s all ease-in-out;
          opacity: 0;
          visibility: hidden;
          border-radius: 0.5rem;
          padding: 0.25rem 0.75rem;
          min-width: 10rem;
          top: 110%;
          right: 0;
          z-index: 1;
        }

        &:hover {
          .dropdown {
            opacity: 1;
            height: max-content;
            visibility: visible;
          }
        }
      }

      .wrap__cart {
        width: 4rem;
        height: 4rem;
        background-color: ${COLORS.primaryColor};
        right: -7.5%;
        top: -18%;
        &:hover {
          transition: 0.15s linear;
          filter: drop-shadow(0px 2px 3px rgba(254, 93, 31, 0.7));
        }

        .icon {
          background-image: url(${images.ic_cart.src});
          width: 2.4rem;
          height: 2.4rem;
          background-repeat: no-repeat;
          background-size: contain;
          .span {
            width: 1.2rem;
            height: 1.2rem;
            bottom: -30%;
            right: -30%;
          }
        }
      }
    }
  }
  .collapse {
    transition: height 0.2s ease-in-out;
    height: 0;
    overflow: hidden;
    background-color: ${COLORS.black95};

    ${(props) =>
      props.collapse
        ? css`
            height: 10rem;
            padding-bottom: 1rem;
          `
        : css`
            transition: height 0.2s ease-in-out;
            height: 0;
            overflow: hidden;
          `}
  }

  .extend__header {
    .wrap {
      max-width: 80%;
      .block__input {
        height: 2.8rem;
        .container__dropdown {
          width: 15rem;
          .icon_down {
            background-image: url(${images.chevron_down.src});
            width: 1.2rem;
            height: 1.2rem;
            background-size: cover;
            transition: 0.2s all ease-in-out;
            ${(props) =>
              props.showDropdown &&
              css`
                transform: rotate(180deg);
              `};
          }

          .dropdown__list {
            top: 120%;
            width: 100%;
            left: -0.75rem;
          }
        }

        input {
          outline: none;
        }
        .icon_search {
          right: 0;
          top: 50%;
          transform: translate(-50%, -50%);
        }
      }
    }
  }

  @media (min-width: ${SIZES.screen_md}) {
    .header {
      .menu {
        max-height: none;
      }
      .menu-icon {
        display: none;
      }
    }
  }

  .header .menu-icon {
    cursor: pointer;
    padding: 1.5rem 1.2rem;
    user-select: none;
  }

  .header .menu-icon .navicon {
    background: white;
    display: block;
    height: 2.5px;
    position: relative;
    transition: background 0.2s ease-out;
    width: 1.5rem;
  }

  .header .menu-icon .navicon:before,
  .header .menu-icon .navicon:after {
    background: white;
    content: '';
    display: block;
    height: 100%;
    position: absolute;
    transition: all 0.2s ease-out;
    width: 100%;
  }

  .header .menu-icon .navicon:before {
    top: 7px;
  }

  .header .menu-icon .navicon:after {
    top: -7px;
  }

  /* menu btn */
  .header .menu-btn {
    display: none;
  }

  .header .menu-btn:checked ~ .menu-icon .navicon {
    background: transparent;
  }

  .header .menu-btn:checked ~ .menu-icon .navicon:before {
    transform: rotate(-45deg);
  }

  .header .menu-btn:checked ~ .menu-icon .navicon:after {
    transform: rotate(45deg);
  }

  .header .menu-btn:checked ~ .menu-icon:not(.steps) .navicon:before,
  .header .menu-btn:checked ~ .menu-icon:not(.steps) .navicon:after {
    top: 0;
  }
`;
