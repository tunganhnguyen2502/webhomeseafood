export const menuNav = [
  {
    label: 'Trang chủ',
    href: '/',
  },
  {
    label: 'Sản phẩm',
    href: '/product',
    child: [
      {
        label: 'Ốc, sò',
      },
      {
        label: 'Tôm, cua, ghẹ, cá',
      },
      {
        label: 'Bạch tuộc, mực ống',
      },
    ],
  },
  {
    label: 'Khuyến mãi',
    href: '/',
  },
  {
    label: 'Liên hệ',
    href: '/',
  },
];
