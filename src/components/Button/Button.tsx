import { COLORS } from '@/constants/themes';
import React, { forwardRef } from 'react';
import styled, { css } from 'styled-components';

interface ButtonProps
  extends React.DetailedHTMLProps<
    React.ButtonHTMLAttributes<HTMLButtonElement>,
    HTMLButtonElement
  > {
  className?: string;
  text?: string;
  colorText?: string;
  colorHoverText?: string;
  fontFamily?: string;
  fontSize?: string;
  fontWeight?: string;
  bgColor?: string;
  upperCase?: boolean;
  bgHoverColor?: string;
  width?: string;
  height?: string;
  border?: string;
  borderRadius?: string;
  boxShadow?: string;
  styleProps?: object;
  disabled?: boolean;
  handleClick?: () => void;
}

const WrapperButton = styled.button<ButtonProps>`
  color: ${(props) => props.colorText};
  background-color: ${(props) => props.bgColor};
  width: ${(props) => props.width};
  height: ${(props) => props.height};
  border: ${(props) => props.border};
  border-radius: ${(props) => props.borderRadius};
  cursor: ${(props) => (props.disabled ? 'not-allowed' : 'pointer')};
  opacity: ${(props) => (props.disabled ? 0.5 : 1)};
  box-shadow: ${(props) => (props.boxShadow ? props.boxShadow : 'unset')};
  transition: 0.15s all ease-in-out;

  ${(props) =>
    props.bgHoverColor
      ? css`
          &:hover {
            transition: 0.15s all ease-in-out;
            background-color: ${props.bgHoverColor};
          }
        `
      : css`
          &:hover {
            animation-name: div-text-hover;
            animation-duration: 2s;
            animation-fill-mode: forwards;
            animation-iteration-count: infinite;
            animation-timing-function: ease;

            filter: drop-shadow(0px 2px 3px rgba(254, 93, 31, 0.7));
            transition: 0.15s all ease-in-out;
          }

          @keyframes div-text-hover {
            0% {
              transition: 0.15s all ease-in-out;
              background: linear-gradient(180deg, #ff7945, #f04e0d);
            }
            100% {
              transition: 0.15s all ease-in-out;
              background: linear-gradient(180deg, #ff7945, #f04e0d);
            }
          }
        `}

  .title__btn {
    color: ${(props) => props.colorText};
    font-size: ${(props) => props.fontSize};
    text-transform: ${(props) => (props.upperCase ? 'uppercase' : 'unset')};
    transition: 0.15s all ease-in-out;
    font-weight: ${(props) => props.fontWeight};
  }
  &:hover {
    .title__btn {
      color: ${(props) => (props.colorHoverText ? props.colorHoverText : props.colorText)};
    }
  }
`;

const Button = forwardRef<HTMLButtonElement, ButtonProps>(
  (
    {
      text = 'Button',
      colorText = '#ffffff',
      bgColor = COLORS.primaryColor,
      width = '100%',
      fontFamily = 'SFProDisplay',
      height = '2.25rem',
      border = `1px solid ${COLORS.primaryStroke}`,
      borderRadius = '5px',
      disabled = false,
      boxShadow,
      bgHoverColor,
      fontSize = '1rem',
      upperCase = false,
      fontWeight = 'normal',
      type = 'submit',
      children,
      className,
      handleClick,
      styleProps,
    },
    buttonRef
  ) => {
    return (
      <WrapperButton
        className={`button__primary flex justify-center items-center ${className}}`}
        type={type}
        ref={buttonRef}
        onClick={handleClick}
        text={text}
        colorText={colorText}
        bgColor={bgColor}
        width={width}
        height={height}
        border={border}
        borderRadius={borderRadius}
        disabled={disabled}
        boxShadow={boxShadow}
        bgHoverColor={bgHoverColor}
        fontSize={fontSize}
        upperCase={upperCase}
        fontWeight={fontWeight}
        style={{ ...styleProps }}
      >
        <p className={`title__btn ${fontFamily}`}>{text}</p>
        {children}
      </WrapperButton>
    );
  }
);

Button.displayName = 'Button';

// const {

// } = props;

export default Button;
