import Button from '@/components/Button';
import { images } from '@/constants/images';
import { Container, Grid } from '@material-ui/core';
import Image from 'next/image';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { CardTitleImage, Wrapper } from './styles';

const HomeBanner = ({}) => {
  const { t: translation } = useTranslation('common');
  const [zIndex, setZIndex] = useState({
    card1: 3,
    card2: 2,
    card3: 1,
  });

  return (
    <Wrapper>
      <Container maxWidth="xl">
        <Grid container>
          <Grid item md={5}>
            <p className="title__baner text-xl6 md:mt-7 dark:text-white text-primaryColor transition">
              {translation('home.title__banner')}
            </p>
            <p className="dark:text-black50 text-black70 transition mt-9">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lobortis enim fames ac in
              velit blandit tincidunt. Tempus feugiat fermentum cras felis eget tempus etiam.
              Potenti massa ipsum et et, nunc aliquam.
            </p>
            <div className="mt-28">
              <Button
                text="Mua Ngay"
                width="14rem"
                height="4rem"
                fontSize="1.5rem"
                upperCase
                fontFamily="SFProDisplayBold"
                fontWeight="800"
                handleClick={() => alert(23123)}
              />
            </div>
          </Grid>
          <Grid item md={7}>
            <section className="flex px-5">
              <CardTitleImage
                bgImage={images.linear__green}
                zIndex={zIndex.card1}
                onClick={() => {
                  setZIndex((prevState) => ({
                    ...prevState,
                    card1: 3,
                    card2: 2,
                    card3: 1,
                  }));
                }}
              >
                <p className="title uppercase text-xl6">Ốc, Sò</p>
                <figure className="wrap__img--food">
                  <Image className="img__food" src={images.food1} alt="food1" />
                </figure>
              </CardTitleImage>

              <CardTitleImage
                bgImage={images.linear__orange}
                zIndex={zIndex.card2}
                onClick={() => {
                  setZIndex((prevState) => ({
                    ...prevState,
                    card1: 1,
                    card2: 3,
                    card3: 2,
                  }));
                }}
              >
                <p className="title uppercase text-xl6">Tôm, cua, {'\r'} ghẹ, cá</p>
                <figure className="wrap__img--food">
                  <Image className="img__food" src={images.food2} alt="food2" />
                </figure>
              </CardTitleImage>

              <CardTitleImage
                bgImage={images.linear__blue}
                zIndex={zIndex.card3}
                onClick={() => {
                  setZIndex((prevState) => ({
                    ...prevState,
                    card1: 1,
                    card2: 2,
                    card3: 3,
                  }));
                }}
              >
                <p className="title uppercase text-xl6">Bạch tuộc, Mực ống</p>
                <figure className="wrap__img--food">
                  <Image className="img__food" src={images.food3} alt="food3" />
                </figure>
              </CardTitleImage>
            </section>
          </Grid>
        </Grid>
        <div className="w-full flex justify-center items-center mt-14">
          <figure className="">
            <Image
              className=""
              src={images.mouse_scroll}
              width="150"
              height="150"
              alt="mouse_scroll"
            />
          </figure>
        </div>
      </Container>
    </Wrapper>
  );
};

export default HomeBanner;
