import styled from 'styled-components';
import { COLORS } from '@/constants/themes';
import React from 'react';

interface CardTitleImageProps {
  bgImage: React.ImgHTMLAttributes<ImageData>;
  zIndex: number;
}

export const Wrapper = styled.div`
  /* padding-top: 10rem; */
  .title__baner {
    font-family: 'UTM_PenumbraBold';
  }
`;

export const CardTitleImage = styled.div<CardTitleImageProps>`
  width: 25rem;
  height: 28rem;
  background-image: url(${(props) => props.bgImage.src});
  background-repeat: no-repeat;
  background-size: contain;
  position: relative;
  cursor: pointer;
  z-index: ${(props) => props.zIndex};
  transition: 0.25s all ease-in-out;
  .title {
    position: absolute;
    top: 0.5rem;
    left: 1.5rem;
    font-family: 'SFProDisplayRegular';
    line-height: 57px;
    letter-spacing: 0.02em;
    transition: 0.25s all ease-in-out;
    color: ${COLORS.white};
  }

  .wrap__img--food {
    position: absolute;
    bottom: -15%;
    left: 0;
    width: 19rem;
    height: auto;
    .img__food {
      width: 100%;
      height: 100%;
      transition: 0.25s all ease-in-out;
      z-index: ${(props) => props.zIndex};
    }
  }
`;
