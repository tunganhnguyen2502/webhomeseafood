import { useAppSelector } from '@/apps/hook';
import { images } from '@/constants/images';
import { selectAppLayoutTheme } from '@/features/appLayout/appLayoutSlice';
import { Container } from '@material-ui/core';
import Image from 'next/image';
import React from 'react';
import SwiperCore, { Navigation } from 'swiper';
import 'swiper/css';
import 'swiper/css/navigation';
import { Swiper, SwiperSlide } from 'swiper/react';
import { dataListComment } from './data';
import { BlockComment, Wrapper } from './styles';

SwiperCore.use([Navigation]);

interface Props {
  data?: {
    test: boolean;
    [key: string]: string | number | object | boolean | Array<object>;
  }[];
}

const HomeCommentSlide: React.FC<Props> = ({ data = [] }) => {
  const navigationPrevRef = React.useRef(null);
  const navigationNextRef = React.useRef(null);
  const appLayoutTheme = useAppSelector(selectAppLayoutTheme);

  return (
    <Wrapper>
      <Container maxWidth="lg" className="relative">
        <div className="w-full flex justify-center items-center">
          <figure className="flex items-center">
            <Image className="img__food" src={images.decor_line_left} alt="food1" />
          </figure>
          <p className="dark:text-white text-primaryColor transition uppercase text-xl6 font-UTM_PenumbraBold mx-4">
            Nhận xét từ khách hàng
          </p>
          <figure className="flex items-center">
            <Image className="img__food" src={images.decor_line_right} alt="food1" />
          </figure>
        </div>
        <Swiper
          className="mt-5"
          spaceBetween={15}
          slidesPerView={2}
          keyboard
          //   navigation={true}
          navigation={{
            prevEl: navigationPrevRef.current,
            nextEl: navigationNextRef.current,
          }}
          breakpoints={{
            320: {
              slidesPerView: 1,
            },
            768: {
              slidesPerView: 2,
            },
          }}
          onSlideChange={() => console.log('slide change')}
          onSwiper={(swiper) => console.log(swiper)}
        >
          {!data ||
            (data.length <= 0 &&
              dataListComment.map((x, i) => {
                return (
                  <SwiperSlide key={i}>
                    <BlockComment
                      className="block__comment dark:bg-black80 bg-white0 transition rounded-lg p-4 flex justify-between items-center"
                      avatarImg={x.avatar}
                      appLayoutTheme={appLayoutTheme}
                    >
                      <div className="flex flex-col justify-center items-center ml-4">
                        <div className="avatar" />
                        <p
                          className="dark:text-white text-black70 transition mt-5 text-lg text-center"
                          style={{ width: '10rem', maxWidth: '13rem' }}
                        >
                          {x.name}
                        </p>
                      </div>
                      <div className="pl-10 pr-3">
                        <div className="quote" />
                        <p className="desc text-sm text-black50 mt-4">{x.desc}</p>
                      </div>
                    </BlockComment>
                  </SwiperSlide>
                );
              }))}
        </Swiper>
        <div className="nav__prev absolute cursor-pointer" ref={navigationPrevRef} />
        <div className="nav__next absolute cursor-pointer" ref={navigationNextRef} />
      </Container>
    </Wrapper>
  );
};

export default HomeCommentSlide;
