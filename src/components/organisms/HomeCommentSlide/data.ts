import { images } from '@/constants/images';

export const dataListComment = [
  {
    name: 'Ronald Richards',
    desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer mauris malesuada dui lobortis faucibus lectus amet semper. Arcu eget dictumst egestas bibendum pretium morbi sed sit in. Proin arcu nisi venenatis, rhoncus. Maecenas neque aliquam mi at enim vel purus amet. Massa vel viverra.',
    avatar: images.avatar1,
  },
  {
    name: 'Devon Lane',
    desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer mauris malesuada dui lobortis faucibus lectus amet semper. Arcu eget dictumst egestas bibendum pretium morbi sed sit in. Proin arcu nisi venenatis, rhoncus. Maecenas neque aliquam mi at enim vel purus amet. Massa vel viverra.',
    avatar: images.avatar2,
  },
  {
    name: 'Ronald Richards',
    desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer mauris malesuada dui lobortis faucibus lectus amet semper. Arcu eget dictumst egestas bibendum pretium morbi sed sit in. Proin arcu nisi venenatis, rhoncus. Maecenas neque aliquam mi at enim vel purus amet. Massa vel viverra.',
    avatar: images.avatar1,
  },
  {
    name: 'Ronald Richards',
    desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer mauris malesuada dui lobortis faucibus lectus amet semper. Arcu eget dictumst egestas bibendum pretium morbi sed sit in. Proin arcu nisi venenatis, rhoncus. Maecenas neque aliquam mi at enim vel purus amet. Massa vel viverra.',
    avatar: images.avatar1,
  },
  {
    name: 'Ronald Richards',
    desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer mauris malesuada dui lobortis faucibus lectus amet semper. Arcu eget dictumst egestas bibendum pretium morbi sed sit in. Proin arcu nisi venenatis, rhoncus. Maecenas neque aliquam mi at enim vel purus amet. Massa vel viverra.',
    avatar: images.avatar1,
  },
  {
    name: 'Ronald Richards',
    desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer mauris malesuada dui lobortis faucibus lectus amet semper. Arcu eget dictumst egestas bibendum pretium morbi sed sit in. Proin arcu nisi venenatis, rhoncus. Maecenas neque aliquam mi at enim vel purus amet. Massa vel viverra.',
    avatar: images.avatar1,
  },
];
