import { images } from '@/constants/images';
import { APP_LAYOUT_THEME, COLORS } from '@/constants/themes';
import styled from 'styled-components';

interface BlockCommentProps {
  avatarImg: React.ImgHTMLAttributes<ImageData>;
  appLayoutTheme: string;
}

export const Wrapper = styled.div`
  margin-top: 3rem;
  .swiper {
    padding: 0 0 2rem 0;
    .swiper-button-prev,
    .swiper-button-next {
      width: 0.5rem;
      color: gray;
    }
    .swiper-button-prev {
      left: -10%;
    }
  }

  .nav__prev {
    background-image: url(${images.chevron_left.src});
    width: 4rem;
    height: 4rem;
    background-size: cover;
    top: 43%;
    left: -5%;
  }
  .nav__next {
    background-image: url(${images.chevron_right.src});
    width: 4rem;
    height: 4rem;
    background-size: cover;
    top: 43%;
    right: -5%;
  }
`;

export const BlockComment = styled.section<BlockCommentProps>`
  user-select: none;
  position: relative;

  &::before {
    position: absolute;
    display: block;
    content: '';
    width: 0;
    height: 0;
    border-left: 40px solid transparent;
    border-right: 0px solid transparent;
    border-top: 40px solid
      ${(props) =>
        props.appLayoutTheme === APP_LAYOUT_THEME.dark ? COLORS.black80 : COLORS.white0};
    bottom: -12%;
    left: 7%;
    transform: rotate(-16deg);
    transition: 0.15s all ease-in-out;
  }
  .avatar {
    background-image: url(${(props) => props.avatarImg.src});
    background-repeat: no-repeat;
    background-size: contain;
    width: 7.5rem;
    height: 7.5rem;
  }

  .quote {
    background-image: url(${images.double_quote.src});
    background-repeat: no-repeat;
    background-size: contain;
    width: 1.6rem;
    height: 1.35rem;
  }

  .desc {
    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-line-clamp: 8;
    -webkit-box-orient: vertical;
  }
`;
