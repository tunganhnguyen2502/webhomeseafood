import { useAppSelector } from '@/apps/hook';
import Button from '@/components/Button';
import Dropdown from '@/components/Dropdown';
import InputField from '@/components/InputField';
import { images } from '@/constants/images';
import { APP_LAYOUT_THEME, COLORS } from '@/constants/themes';
import { selectAppLayoutTheme } from '@/features/appLayout/appLayoutSlice';
import { Form, Formik, FormikValues } from 'formik';
import Image from 'next/image';
import { useRouter } from 'next/router';
import React, { useState } from 'react';
import { Wrapper } from './styles';

interface FormPaymentProps {
  tst?: boolean;
}

interface PaymentSelectedState {
  id: number | string;
  code: string;
  img: React.ImgHTMLAttributes<ImageData>;
}

const FormPayment: React.FC<FormPaymentProps> = ({}) => {
  const appLayoutTheme = useAppSelector(selectAppLayoutTheme);
  const router = useRouter();

  const [paymentSelected, setPaymentSelected] = useState<PaymentSelectedState>(listPayment[0]);

  const _handleSubmit = (value: FormikValues) => {
    console.log('value submit: ', value);
    router.push('/payment-success');
  };

  return (
    <Wrapper>
      <div className="information mt-2 pr-8">
        <p className="text-xl3 dark:text-white text-black90 uppercase font-UTM_PenumbraBold">
          Thời gian giao hàng
        </p>

        <Formik initialValues={{ note: '' }} onSubmit={_handleSubmit}>
          <Form className="mt-10">
            <div className="flex justify-between w-full mt-10">
              <div className="w-full mr-5">
                <p className="mb-1 dark:text-white text-black70 transition">Ngày giao hàng</p>
                <Dropdown
                  placeholder="Ngày giao hàng"
                  listData={[{ name: 'Phường A' }, { name: 'Phường B' }, { name: 'Phường C' }]}
                />
              </div>

              <div className="w-full">
                <p className="mb-1 dark:text-white text-black70 transition">Giờ giao hàng</p>
                <Dropdown
                  placeholder="Giờ giao hàng"
                  listData={[{ name: 'Quận A' }, { name: 'Quận B' }, { name: 'Quận C' }]}
                />
              </div>
            </div>
            <div className="flex justify-between w-full mt-10">
              <div className="w-full">
                <InputField
                  color={appLayoutTheme === APP_LAYOUT_THEME.dark ? COLORS.white : COLORS.black70}
                  name="note"
                  label={<p className="mb-1 dark:text-white text-black70 transition">Ghi chú</p>}
                  placeholder="Nhập lưu ý của bạn với cửa hàng nhé"
                  type="text"
                />
              </div>
            </div>

            <p className="text-xl3 dark:text-white text-black90 uppercase font-UTM_PenumbraBold mt-16">
              Phương thức thanh toán
            </p>
            <div className="flex justify-start items-center w-full mt-4 mb-20">
              {listPayment.map((x) => {
                return (
                  <figure
                    key={x.id}
                    className="wrapper_img--payment cursor-pointer relative"
                    onClick={() => setPaymentSelected(x)}
                  >
                    <div className="mr-3">
                      <Image
                        className="img__type"
                        src={x.img}
                        alt="img__payment"
                        width={180}
                        height={108}
                      />
                    </div>
                    <p className="text absolute dark:text-white0 text-black90 pr-3">{x.text}</p>
                    {paymentSelected?.code === x.code && (
                      <div className="ic__check absolute">
                        <Image
                          src={images.ic_check_payment_cart}
                          alt="ic__check"
                          width={33}
                          height={33}
                        />
                      </div>
                    )}
                  </figure>
                );
              })}
            </div>

            <div className="mt-5">
              <p className="dark:text-white text-black70 transition text-sm my-5">
                Nhấn &quot;Xác nhận thông tin&quot; đồng nghĩa với bạn đã đồng ý với các{' '}
                <span className="text-primaryColor cursor-pointer">
                  điều kiện và điều khoản thanh toán
                </span>
              </p>
              <Button
                height="4rem"
                width="14rem"
                fontSize="1.2rem"
                fontWeight="bold"
                text="Tiếp tục"
              />
            </div>
          </Form>
        </Formik>
      </div>
    </Wrapper>
  );
};

const listPayment = [
  {
    id: 0,
    code: 'cash',
    text: 'Thanh toán tiền mặt',
    img: images.payment_cash,
  },
  {
    id: 1,
    code: 'atm',
    text: 'Chuyển khoản để thanh toán',
    img: images.payment_atm,
  },
  {
    id: 2,
    code: 'momo',
    text: 'Thanh toán bằng ví điện tử',
    img: images.payment_ewallet,
  },
];

export default FormPayment;
