import styled from 'styled-components';

export const Wrapper = styled.div`
  .wrapper_img--payment {
    .img__type {
      border-radius: 0.75rem;
    }
    .ic__check {
      bottom: 0.5rem;
      right: 1.25rem;
    }

    .text {
      top: 1rem;
      left: 1.25rem;
    }
  }
`;
