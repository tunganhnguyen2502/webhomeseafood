import CartProductItem from '@/components/molecules/Cart/CartProductItem';
import { images } from '@/constants/images';
import React from 'react';
import { Wrapper } from './styles';

interface Props {
  tst?: boolean;
}

const SideDish: React.FC<Props> = ({}) => {
  return (
    <Wrapper>
      <p className="text-xl3 dark:text-white text-black90 transition font-UTM_PenumbraBold">
        Món dùng kèm
      </p>
      <div className="wrap__list mt-4 flex overflow-auto">
        {listCartProduct.map((x, i) => {
          return (
            <CartProductItem key={i} imgProduct={x.image} disabled={x.disabled} className="mr-3" />
          );
        })}
      </div>
    </Wrapper>
  );
};

const listCartProduct = [
  {
    image: images.gettyimages1.src,
    disabled: true,
  },
  {
    image: images.gettyimages2.src,
    disabled: false,
  },
  {
    image: images.gettyimages3.src,
    disabled: false,
  },
  {
    image: images.gettyimages4.src,
    disabled: true,
  },
];

export default SideDish;
