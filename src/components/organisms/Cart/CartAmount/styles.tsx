import { COLORS } from '@/constants/themes';
import styled from 'styled-components';

export const Wrapper = styled.section`
  .wrap__list {
    height: 40rem;
    ::-webkit-scrollbar-track {
      opacity: 1;
    }

    ::-webkit-scrollbar {
      width: 7px;
      background-color: ${COLORS.black70};
    }

    ::-webkit-scrollbar-thumb {
      border-radius: 30px;
      box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
      -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
      background-color: ${COLORS.black20};
    }
    .wrap__item {
      width: 100%;
      min-width: 100%;
    }
  }
`;
