import { useAppSelector } from '@/apps/hook';
import Button from '@/components/Button';
import InputField from '@/components/InputField';
import CartProductItem from '@/components/molecules/Cart/CartProductItem';
import { images } from '@/constants/images';
import { APP_LAYOUT_THEME, COLORS } from '@/constants/themes';
import { selectAppLayoutTheme } from '@/features/appLayout/appLayoutSlice';
import { convertNumToMoney } from '@/utils/convertNumberToMoney';
import { Form, Formik, FormikValues } from 'formik';
import React from 'react';
import { Wrapper } from './styles';

interface Props {
  tst?: boolean;
}

const CartAmount: React.FC<Props> = ({}) => {
  const appLayoutTheme = useAppSelector(selectAppLayoutTheme);

  const _handleSubmitPromotionCode = (value: FormikValues) => {
    console.log('promotionCode submit: ', value);
  };

  return (
    <Wrapper>
      <div className="rounded-lg py-2 dark:bg-black100 bg-white90 transition">
        <p className="text-xl3 dark:text-white text-black90 transition font-UTM_PenumbraBold px-3">
          Giỏ hàng
        </p>
        <div className="wrap__list overflow-auto px-3">
          {listCartProduct.map((x, i) => {
            return (
              <CartProductItem
                defaultQty={x.unit}
                key={i}
                imgProduct={x.image}
                className="wrap__item mt-3"
              />
            );
          })}
        </div>
      </div>

      <div className="cart__amount mt-4">
        <div className="flex justify-between items-center">
          <Formik initialValues={{ promotionCode: '' }} onSubmit={_handleSubmitPromotionCode}>
            <Form className="w-full">
              <div className="flex justify-between items-center w-full">
                <div className="w-full mr-3">
                  <InputField
                    name="promotionCode"
                    placeholder="Nhập mã khuyến mãi"
                    type="text"
                    color={appLayoutTheme === APP_LAYOUT_THEME.dark ? COLORS.white : COLORS.black70}
                  />
                </div>
                <Button
                  text="Áp dụng mã"
                  width="12rem"
                  height="3rem"
                  borderRadius="5px"
                  bgColor={COLORS.white}
                  bgHoverColor={COLORS.white}
                  colorText={COLORS.primaryColor}
                  border="none"
                  fontSize="1.25rem"
                />
              </div>
            </Form>
          </Formik>
        </div>

        <div className="mt-10">
          <div className="flex justify-between items-center">
            <p className="text-black50 font-SFProDisplayRegular text-lg">Tổng</p>
            <p className="font-SFProDisplayRegular dark:text-white text-black70 transition">
              {convertNumToMoney({ number: 1234567 })}
            </p>
          </div>
          <div className="flex justify-between items-center mt-2">
            <p className="text-black50 font-SFProDisplayRegular text-lg">Khuyến mãi</p>
            <p className="font-SFProDisplayRegular dark:text-white text-black70 transition">
              -{convertNumToMoney({ number: 0 })}
            </p>
          </div>
          <div className="flex justify-between items-center mt-2">
            <p className="text-black50 font-SFProDisplayRegular text-lg">Phí vận chuyển</p>
            <p className="font-SFProDisplayRegular dark:text-white text-black70 transition">
              {convertNumToMoney({ number: 20000 })}
            </p>
          </div>
          <div className="flex justify-between items-center mt-5">
            <p className="text-black50 font-SFProDisplayRegular text-lg">Tạm tính</p>
            <p className="font-SFProDisplayBold text-lg text-primaryColor">
              {convertNumToMoney({ number: 12345678 })}
            </p>
          </div>
        </div>
      </div>
    </Wrapper>
  );
};

const listCartProduct = [
  {
    image: images.gettyimages1.src,
    unit: 1,
  },
  {
    image: images.gettyimages2.src,
    unit: 3,
  },
  {
    image: images.gettyimages3.src,
    unit: 1,
  },
  {
    image: images.gettyimages4.src,
    unit: 1,
  },
  {
    image: images.gettyimages1.src,
    unit: 5,
  },
  {
    image: images.gettyimages2.src,
    unit: 1,
  },
  {
    image: images.gettyimages3.src,
    unit: 8,
  },
  {
    image: images.gettyimages4.src,
    unit: 12,
  },
  {
    image: images.gettyimages2.src,
    unit: 4,
  },
  {
    image: images.gettyimages3.src,
    unit: 2,
  },
];

export default CartAmount;
