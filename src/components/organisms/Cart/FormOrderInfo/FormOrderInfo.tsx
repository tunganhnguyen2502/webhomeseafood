import { useAppSelector } from '@/apps/hook';
import Button from '@/components/Button';
import Dropdown from '@/components/Dropdown';
import InputField from '@/components/InputField';
import TagSelection from '@/components/TagSelection';
import { images } from '@/constants/images';
import { APP_LAYOUT_THEME, COLORS } from '@/constants/themes';
import { selectAppLayoutTheme } from '@/features/appLayout/appLayoutSlice';
import { Form, Formik, FormikValues } from 'formik';
import { useRouter } from 'next/router';
import React, { useState } from 'react';
import { Wrapper } from './styles';
interface Props {
  tst?: boolean;
}

interface DeliveryState {
  type: 'shipping' | 'shop' | '';
}

const deliveryStateData = {
  shipping: 'shipping',
  shop: 'shop',
};

const FormOrderInfo: React.FC<Props> = ({}) => {
  const appLayoutTheme = useAppSelector(selectAppLayoutTheme);
  const initialValues = { name: '', phone: '', address: '', email: '' };
  const router = useRouter();

  const [delivery, setDelivery] = useState<DeliveryState>({ type: 'shipping' });

  const _handleSubmit = (value: FormikValues) => {
    console.log('value submit: ', value);
    router.push('/cart-payment');
  };

  return (
    <Wrapper>
      <div className="information mt-5 pr-16">
        <div className="delivery mt-24">
          <p className="text-xl3 dark:text-white text-black90 uppercase font-UTM_PenumbraBold">
            Hình thức giao - nhận
          </p>
          <div className="flex jusity-between items-center mt-10 w-full">
            <div className="mr-4">
              <TagSelection
                appLayoutTheme={appLayoutTheme}
                icon={
                  appLayoutTheme === APP_LAYOUT_THEME.dark
                    ? images.ic_shipping_default
                    : images.ic_shipping_gray
                }
                iconActive={
                  appLayoutTheme === APP_LAYOUT_THEME.dark
                    ? images.ic_shipping_active
                    : images.ic_shipping_default
                }
                title="Giao hàng tận nhà"
                active={delivery.type === deliveryStateData.shipping}
                onClick={() => setDelivery({ type: 'shipping' })}
              />
            </div>
            <TagSelection
              appLayoutTheme={appLayoutTheme}
              icon={
                appLayoutTheme === APP_LAYOUT_THEME.dark
                  ? images.ic_shop_default
                  : images.ic_shop_gray
              }
              iconActive={
                appLayoutTheme === APP_LAYOUT_THEME.dark
                  ? images.ic_shop_active
                  : images.ic_shop_default
              }
              title="Đến nhận tại cửa hàng"
              active={delivery.type === deliveryStateData.shop}
              onClick={() => setDelivery({ type: 'shop' })}
            />
          </div>
        </div>

        <div className="flex justify-between items-center mt-24">
          <p className="text-xl3 dark:text-white text-black90 uppercase font-UTM_PenumbraBold">
            Thông tin đơn hàng
          </p>
          <div>
            <Dropdown
              placeholder="Quận B"
              listData={[
                { name: 'Quận AAAA AA AAAAA AAA AA AA AA AAA' },
                { name: 'Quận B' },
                { name: 'Quận C' },
              ]}
            />
          </div>
        </div>

        <Formik initialValues={initialValues} onSubmit={_handleSubmit}>
          <Form className="mt-5">
            <div className="flex justify-between w-full">
              <div className="mr-5 w-full">
                <InputField
                  color={appLayoutTheme === APP_LAYOUT_THEME.dark ? COLORS.white : COLORS.black70}
                  name="name"
                  label={<p className="mb-1 dark:text-white text-black70 transition">Người nhận</p>}
                  placeholder="Họ và tên"
                  type="text"
                />
              </div>
              <InputField
                color={appLayoutTheme === APP_LAYOUT_THEME.dark ? COLORS.white : COLORS.black70}
                name="phone"
                label={
                  <p className="mb-1 dark:text-white text-black70 transition">Số điện thoại</p>
                }
                placeholder="Vd:012345678"
                type="number"
                maxLength={10}
              />
            </div>

            <div className="w-full mt-10">
              <InputField
                color={appLayoutTheme === APP_LAYOUT_THEME.dark ? COLORS.white : COLORS.black70}
                name="address"
                label={
                  <p className="mb-1 dark:text-white text-black70 transition">Địa chỉ nhận hàng</p>
                }
                placeholder="Nhập số nhà và tên đường"
                type="text"
              />
            </div>
            <div className="flex justify-between w-full mt-10">
              <div className="w-full mr-5">
                <p className="mb-1 dark:text-white text-black70 transition">Phường/ Xã</p>
                <Dropdown
                  placeholder="Phường A"
                  listData={[{ name: 'Phường A' }, { name: 'Phường B' }, { name: 'Phường C' }]}
                />
              </div>

              <div className="w-full">
                <p className="mb-1 dark:text-white text-black70 transition">Quận/ Huyện</p>
                <Dropdown
                  placeholder="Quận A"
                  listData={[{ name: 'Quận A' }, { name: 'Quận B' }, { name: 'Quận C' }]}
                />
              </div>
            </div>
            <div className="flex justify-between w-full mt-10">
              <div className="w-full">
                <p className="mb-1 dark:text-white text-black70 transition">Tỉnh/ Thành phố</p>
                <Dropdown
                  placeholder="Thành Phố A"
                  listData={[
                    { name: 'Thành Phố A' },
                    { name: 'Thành Phố B' },
                    { name: 'Thành Phố C' },
                  ]}
                />
              </div>
            </div>

            <div className="mt-10">
              <Button
                height="4rem"
                width="14rem"
                fontSize="1.2rem"
                fontWeight="bold"
                text="Tiếp tục"
              />
            </div>
          </Form>
        </Formik>
      </div>
    </Wrapper>
  );
};

export default FormOrderInfo;
