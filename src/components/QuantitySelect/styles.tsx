import { APP_LAYOUT_THEME, COLORS, SIZES } from '@/constants/themes';
import styled from 'styled-components';

interface WrapperProps {
  isDarkStyle?: boolean;
  appLayoutTheme?: string;
}

export const Wrapper = styled.div<WrapperProps>`
  user-select: none;
  .qty__default {
    .wrap__plus {
      width: 3rem;
      height: 3rem;
      border-radius: 50%;
      background-color: ${COLORS.primaryColor};
      cursor: pointer;
      transition: background 0.25s linear;

      &:hover {
        transition: background 0.25s linear;
        background-position: 0 0;
        filter: drop-shadow(0px 2px 3px rgba(254, 93, 31, 0.7));
      }

      .ic__plus {
        user-select: none;
      }
    }
  }

  .qty__active {
    width: 10rem;
    height: 3rem;
    background-color: ${(props) =>
      props.appLayoutTheme === APP_LAYOUT_THEME.dark && props.isDarkStyle
        ? COLORS.black90
        : props.appLayoutTheme === APP_LAYOUT_THEME.dark && !props.isDarkStyle
        ? COLORS.primaryColor
        : props.appLayoutTheme === APP_LAYOUT_THEME.light && COLORS.white90};
    border-radius: 2.5rem;
    .wrap__plus,
    .wrap__minus {
      cursor: pointer;
      width: 3rem;
      height: 3rem;
      .ic__minus,
      .ic__plus {
        user-select: none;
      }
    }

    .wrap__input {
      input {
        width: 3rem;
        height: 3rem;
        background: transparent;
        font-size: ${SIZES.xl2};
        color: ${(props) =>
          props.appLayoutTheme === APP_LAYOUT_THEME.dark && props.isDarkStyle
            ? COLORS.primaryColor
            : props.appLayoutTheme === APP_LAYOUT_THEME.dark && !props.isDarkStyle
            ? COLORS.white90
            : props.appLayoutTheme === APP_LAYOUT_THEME.light && COLORS.primaryColor};

        /* color: ${(props) => (props.isDarkStyle ? COLORS.primaryColor : COLORS.white)}; */
        text-align: center;
        border-radius: 0;
        outline: none;
        font-size: 1.3rem;
        font-weight: 700;
      }

      input::-webkit-outer-spin-button,
      input::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
      }

      input[type='number'] {
        -moz-appearance: textfield;
      }
    }
  }
`;
