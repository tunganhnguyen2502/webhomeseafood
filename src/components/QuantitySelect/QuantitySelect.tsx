import { useAppSelector } from '@/apps/hook';
import { images } from '@/constants/images';
import { APP_LAYOUT_THEME } from '@/constants/themes';
import { selectAppLayoutTheme } from '@/features/appLayout/appLayoutSlice';
import useDebounce from '@/hooks/useDebounce';
import Image from 'next/image';
import React, { useEffect, useState } from 'react'; // useState
import { Wrapper } from './styles';

interface Props {
  defaultQty: number;
  getValue: (value: number) => void;
  timeDebounce?: number;
  className?: string;
  isDarkStyle?: boolean;
  lowest?: number;
  limit?: number;
}

const QuantitySelect = ({
  className = '',
  timeDebounce = 200,
  getValue,
  isDarkStyle = false,
  lowest = 0,
  limit = 100,
  defaultQty = 0,
}: Props) => {
  const appLayoutTheme = useAppSelector(selectAppLayoutTheme);

  const [qty, setQty] = useState<number>(defaultQty);
  const debounceQty = useDebounce(qty, timeDebounce);

  /**
   * Function
   */
  const _handleChangeQty = (e: React.ChangeEvent<HTMLInputElement>) => {
    e.preventDefault();
    const valueInput = e.target.value;
    let result = Number(valueInput);
    if (result > limit) result = limit;
    setQty(result > 0 ? result : lowest);
  };

  const _handleDecrease = () => {
    let newQty = qty - 1;
    if (newQty <= lowest) newQty = lowest;
    setQty(newQty);
  };

  const _handleIncrease = () => {
    let newQty = qty + 1;
    if (newQty > limit) newQty = limit;
    setQty(newQty);
  };

  const _handleBlur = (e: React.FocusEvent<HTMLInputElement>) => {
    e.preventDefault();
    if (qty < lowest) setQty(lowest);
  };

  /**
   * Effect
   */
  useEffect(() => {
    getValue(debounceQty);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [debounceQty]);

  return (
    <Wrapper className={className} isDarkStyle={isDarkStyle} appLayoutTheme={appLayoutTheme}>
      {qty === 0 && (
        <div className="qty__default" onClick={() => setQty(qty + 1)}>
          <figure className="wrap__plus flex justify-center items-center">
            <Image
              className="ic__plus"
              src={images.ic_plus_default}
              alt="ic__plus"
              width="20"
              height="20"
              objectFit="contain"
            />
          </figure>
        </div>
      )}
      {qty > 0 && (
        <div className="qty__active flex justify-between items-center">
          <figure
            className="wrap__minus flex justify-center items-center"
            onClick={_handleDecrease}
          >
            <Image
              className="ic__minus"
              src={
                appLayoutTheme === APP_LAYOUT_THEME.dark
                  ? images.ic_minus_default
                  : images.ic_minus_black
              }
              alt="ic__minus"
              width="20"
              height="20"
              objectFit="contain"
            />
          </figure>
          <div className="wrap__input">
            <input
              value={qty}
              type="number"
              inputMode="numeric"
              onChange={_handleChangeQty}
              onBlur={_handleBlur}
            />
          </div>

          <figure className="wrap__plus flex justify-center items-center" onClick={_handleIncrease}>
            <Image
              className="ic__plus"
              src={
                appLayoutTheme === APP_LAYOUT_THEME.dark
                  ? images.ic_plus_default
                  : images.ic_plus_black
              }
              alt="ic__plus"
              width="20"
              height="20"
              objectFit="contain"
            />
          </figure>
        </div>
      )}
    </Wrapper>
  );
};

export default QuantitySelect;
