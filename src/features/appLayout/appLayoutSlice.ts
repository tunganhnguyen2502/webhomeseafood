import { RootState } from '@/apps/store';
import { AppTheme } from '@/models/appLayout';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';

export interface AppLayoutState {
  theme: 'dark' | 'light';
}

const initialState: AppLayoutState = {
  theme: 'dark',
};

const appLayoutSlice = createSlice({
  name: 'appLayout',
  initialState: initialState,
  reducers: {
    applyTheme(state, action: PayloadAction<AppTheme>) {
      state.theme = action.payload;
    },
  },
});

// Acitons
export const appLayoutActions = appLayoutSlice.actions;

// Selectors
export const selectAppLayoutTheme = (state: RootState) => state.appLayout.theme;

// Reducers
const appLayoutReducer = appLayoutSlice.reducer;
export default appLayoutReducer;
