import { useAppDispatch } from '@/apps/hook';
import { appLayoutActions } from '@/features/appLayout/appLayoutSlice';
import { useEffect, useState } from 'react';
import { AppTheme } from '../models';
interface ReturnType {
  toggleDarkMode: () => void;
}
const useDarkMode = (): ReturnType => {
  const dispatch = useAppDispatch();

  const [isDarkMode, setIsDarkMode] = useState(
    typeof window !== 'undefined' && localStorage.theme === 'dark'
  );
  const toggleDarkMode = () => setIsDarkMode(!isDarkMode);

  useEffect(() => {
    const html = window.document.documentElement;

    const prevTheme: AppTheme = isDarkMode ? 'light' : 'dark';
    html.classList.remove(prevTheme);

    const nextTheme: AppTheme = isDarkMode ? 'dark' : 'light';
    html.classList.add(nextTheme);

    dispatch(appLayoutActions.applyTheme(nextTheme));
    localStorage.setItem('theme', nextTheme);
  }, [isDarkMode, dispatch]);

  return { toggleDarkMode };
};

export default useDarkMode;
