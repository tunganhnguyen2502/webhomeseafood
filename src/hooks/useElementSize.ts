import { RefObject, useCallback, useLayoutEffect, useState } from 'react';

// See: https://usehooks-ts.com/react-hook/use-event-listener
import useEventListener from './useEventListener';

interface Size {
  width: number;
  height: number;
}

function useElementSize<T extends HTMLElement = HTMLDivElement>(
  elementRef: RefObject<T> | null
): Size {
  const [size, setSize] = useState<Size>({
    width: 0,
    height: 0,
  });

  // Prevent too many rendering using useCallback
  const handleSize = useCallback(() => {
    const node = elementRef?.current;
    if (node) {
      setSize({
        width: node.offsetWidth || 0,
        height: node.offsetHeight || 0,
      });
    }
  }, [elementRef]);

  useEventListener('resize', handleSize);

  // Initial size on mount
  useLayoutEffect(() => {
    handleSize();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return size;
}

export default useElementSize;

// Usage
// export default function Component() {
//     const squareRef = useRef(null)

//     const { width, height } = useElementSize(squareRef)

//     return (
//       <>
//         <p>{`the square width is ${width}px and height ${height}px`}</p>

//         <div
//           ref={squareRef}
//           style={{
//             width: '50%',
//             paddingTop: '50%',
//             backgroundColor: 'aquamarine',
//             margin: 'auto',
//           }}
//         />

//         <p>Try, resize your window</p>
//       </>
//     )
//   }
