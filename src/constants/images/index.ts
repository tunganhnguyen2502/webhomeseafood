// image for test
import gettyimages1 from '../../assets/images/imgTest/gettyimages1.png';
import gettyimages2 from '../../assets/images/imgTest/gettyimages2.png';
import gettyimages3 from '../../assets/images/imgTest/gettyimages3.png';
import gettyimages4 from '../../assets/images/imgTest/gettyimages4.png';
import avatar1 from '../../assets/images/imgTest/avatar1.png';
import avatar2 from '../../assets/images/imgTest/avatar2.png';
/////////////////////

import logo_darkMode from '../../assets/images/common/logo_darkMode.svg';
import logo_lightMode from '../../assets/images/common/logo_lightMode.svg';
import mouse_scroll from '../../assets/images/home/mouse_scroll.gif';
import linear__green from '../../assets/images/home/linear__green.png';
import linear__orange from '../../assets/images/home/linear__orange.png';
import linear__blue from '../../assets/images/home/linear__blue.png';
import food1 from '../../assets/images/home/food1.png';
import food2 from '../../assets/images/home/food2.png';
import food3 from '../../assets/images/home/food3.png';
import decor_line_left from '../../assets/images/home/decor_line_left.png';
import decor_line_right from '../../assets/images/home/decor_line_right.png';
import ic_clock_orange from '../../assets/images/common/ic_clock_orange.png';
import bg_product_darkMode from '../../assets/images/product/bg_product_darkMode.png';
import bg_product_lightMode from '../../assets/images/product/bg_product_lightMode.png';
import chevron_down from '../../assets/images/common/chevron_down.svg';
import ic_minus_default from '../../assets/images/quantity/ic_minus_default.svg';
import ic_plus_default from '../../assets/images/quantity/ic_plus_default.svg';
import ic_minus_black from '../../assets/images/quantity/ic_minus_black.svg';
import ic_plus_black from '../../assets/images/quantity/ic_plus_black.svg';
import ic_cart from '../../assets/images/common/ic_cart.svg';
import double_quote from '../../assets/images/common/double_quote.svg';
import unsplash_bg from '../../assets/images/common/unsplash_bg.png';
import chevron_left from '../../assets/images/common/chevron_left.svg';
import chevron_right from '../../assets/images/common/chevron_right.svg';
import ic_shipping_default from '../../assets/images/common/ic_shipping_default.svg';
import ic_shipping_active from '../../assets/images/common/ic_shipping_active.svg';
import ic_shipping_gray from '../../assets/images/common/ic_shipping_gray.svg';
import ic_shop_default from '../../assets/images/common/ic_shop_default.svg';
import ic_shop_active from '../../assets/images/common/ic_shop_active.svg';
import ic_shop_gray from '../../assets/images/common/ic_shop_gray.svg';
import payment_cash from '../../assets/images/payment/payment_cash.png';
import payment_atm from '../../assets/images/payment/payment_atm.png';
import payment_ewallet from '../../assets/images/payment/payment_ewallet.png';
import ic_check_payment_cart from '../../assets/images/payment/ic_check_payment_cart.svg';
import gif_success from '../../assets/images/common/gif_success.gif';

export const images = {
  // image for test
  gettyimages1,
  gettyimages2,
  gettyimages3,
  gettyimages4,
  avatar1,
  avatar2,
  //////////////////////////

  logo_darkMode,
  logo_lightMode,
  mouse_scroll,
  linear__green,
  linear__orange,
  linear__blue,
  food1,
  food2,
  food3,
  decor_line_left,
  decor_line_right,
  ic_clock_orange,
  bg_product_darkMode,
  bg_product_lightMode,
  chevron_down,
  ic_minus_default,
  ic_plus_default,
  ic_minus_black,
  ic_plus_black,
  ic_cart,
  double_quote,
  unsplash_bg,
  chevron_left,
  chevron_right,
  ic_shipping_default,
  ic_shipping_active,
  ic_shipping_gray,
  ic_shop_default,
  ic_shop_active,
  ic_shop_gray,
  payment_cash,
  payment_atm,
  payment_ewallet,
  ic_check_payment_cart,
  gif_success,
};
