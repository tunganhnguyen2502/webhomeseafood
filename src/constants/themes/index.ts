export const APP_LAYOUT_THEME = {
  dark: 'dark',
  light: 'light',
};

export const COLORS = {
  // base colors
  primaryColor: '#FF6020',
  primaryHover: '#F04E0D',
  primaryStroke: '#F05213',
  primaryBg: '#ff6020',
  lightOrange: '#FFAF8D',
  black20: '#B0B0B0',
  black50: '#858585',
  black70: '#535353',
  black75: '#3D3D3D',
  black80: '#292929',
  black90: '#1E1E1E',
  black95: '#151515',
  black100: '#000000',
  white0: '#FFFFFF',
  white: '#FFF9F9',
  white90: '#ECEAE7',
  white95: '#F1F1F1',
  red: '#DD2929',
  transparent: 'transparent',
};

export const SIZES = {
  // global sizes
  xs: '.75rem', // 12px
  sm: '.875rem', // 14px
  base: '1rem', // 16px
  lg: '1.125rem', // 18px
  xl: '1.25rem', // 20px
  xl1: '1.375rem', // 22px
  xl2: '1.5rem', // 24px
  xl3: '1.625rem', // 26px
  xl4: '1.75rem', // 28px
  xl5: '1.875rem', // 30px
  xl6: '2.25rem', // 36px
  xl7: '3rem',
  xl8: '4rem',
  xl9: '5rem',

  // web screens
  screen_sm: '600px',
  screen_sm1: '768px',
  screen_md: '960px',
  screen_lg: '1280px',
  screen_xl: '1920px',

  // web dimensions
  viewWidth: '100vw',
  viewHeight: '100vh',
  view_sm: 600,
  view_sm1: 768,
  view_md: 960,
  view_lg: 1280,
  view_xl: 1920,
};

export const FONTS_STYLE = {
  detailTextBold: {
    fontFamily: 'SFProDisplay',
    fontStyle: 'normal',
    fontWeight: '600',
    fontSize: SIZES.sm,
    lineHeight: '17px',
    color: COLORS.black100,
  },
  titlePageText: {
    fontFamily: 'SFProDisplay',
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: SIZES.xl2,
    lineHeight: '29px',
    letterSpacing: '0.02em',
    color: COLORS.black100,
  },
  bodyText: {
    fontFamily: 'SFProDisplay',
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: SIZES.xl,
    lineHeight: '24px',
    letterSpacing: '0.02em',
    color: COLORS.black100,
  },
  ctaText: {
    fontFamily: 'SFProDisplay',
    fontStyle: 'normal',
    fontWeight: '800',
    fontSize: SIZES.xl2,
    lineHeight: '29px',
    letterSpacing: '0.02em',
    color: COLORS.black100,
  },
  specialTitleText: {
    fontFamily: 'UTM_Penumbra',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: SIZES.xl6,
    lineHeight: '53px',
    color: COLORS.black100,
  },
  title: {
    fontFamily: 'SFProDisplay',
    fontStyle: 'normal',
    fontWeight: '600',
    fontSize: SIZES.xl2,
    lineHeight: '29px',
    letterSpacing: '0.02em',
    color: COLORS.black100,
  },
  oldPrice: {
    fontFamily: 'SFProDisplay',
    fontStyle: 'normal',
    fontWeight: '600',
    fontSize: SIZES.xl2,
    lineHeight: '29px',
    letterSpacing: '0.02em',
    textDecorationLine: 'line-through',
    color: COLORS.black100,
  },
  discountText: {
    fontFamily: 'SFProDisplay',
    fontStyle: 'normal',
    fontWeight: '800',
    fontSize: SIZES.xl,
    lineHeight: '24px',
    letterSpacing: '0.02em',
    color: COLORS.black100,
  },
  regularTitle: {
    fontFamily: 'SFProDisplay',
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: SIZES.xl2,
    lineHeight: '29px',
    letterSpacing: '0.02em',
    color: COLORS.black100,
  },
};
