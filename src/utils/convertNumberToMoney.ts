interface Params {
  number: number;
  sign?: string;
  unit?: string;
}
export const convertNumToMoney = ({ number = 0, sign = '.', unit = 'đ' }: Params) => {
  if (number < 1000) {
    return `${number}${unit}`;
  }
  const fixedNumber = number > 0 ? number.toFixed() : number;
  const val = fixedNumber.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, `$1${sign}`);
  const res = `${val}${unit}`;
  return res;
};
